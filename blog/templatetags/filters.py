from django import template
register = template.Library()

@register.filter
def return_item(l, i):
    try:
        return l[i]
    except:
        return None

@register.filter
def gettype(l):
    try:
        return type(l)
    except:
        return None


@register.filter
def uni_to_int(l):
    try:
        return int(l)
    except:
        return None

@register.filter
def removeimg(l):
    if l.find("<img src") != -1:
        try:
            return l[:l.index("<img src")]+l[l.index("\" />")+4:]
        except:
            return None
    else:
        return l


@register.filter
def getimg(l):
    try:
        return l[l.index("<img src"):l.index("\" />") + 4]
    except:
        return ""



